﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="InstallQuery" Type="Folder">
			<Item Name="CSVInstallQuery.lvclass" Type="LVClass" URL="../CSVInstallQuery/CSVInstallQuery.lvclass"/>
			<Item Name="DriverInstallQuery.lvclass" Type="LVClass" URL="../DriverInstallQuery/DriverInstallQuery.lvclass"/>
			<Item Name="InstallQuery.lvclass" Type="LVClass" URL="../InstallQuery/InstallQuery.lvclass"/>
			<Item Name="WinRegInstallQuery.lvclass" Type="LVClass" URL="../WinRegInstallQuery/WinRegInstallQuery.lvclass"/>
		</Item>
		<Item Name="QueryData" Type="Folder">
			<Item Name="DriverInstall.lvclass" Type="LVClass" URL="../DriverInstall/DriverInstall.lvclass"/>
			<Item Name="Install.lvclass" Type="LVClass" URL="../Install/Install.lvclass"/>
			<Item Name="SoftwareInstall.lvclass" Type="LVClass" URL="../SoftwareInstall/SoftwareInstall.lvclass"/>
		</Item>
		<Item Name="Report" Type="Folder">
			<Item Name="ExcelReport.lvclass" Type="LVClass" URL="../ExcelReport/ExcelReport.lvclass"/>
			<Item Name="Report.lvclass" Type="LVClass" URL="../Report/Report.lvclass"/>
			<Item Name="ReportPage.lvclass" Type="LVClass" URL="../ReportPage/ReportPage.lvclass"/>
		</Item>
		<Item Name="Support" Type="Folder">
			<Item Name=".rels" Type="Document" URL="../.rels"/>
			<Item Name="theme1.xml" Type="Document" URL="../theme1.xml"/>
		</Item>
		<Item Name="UI" Type="Folder">
			<Item Name="SoftwareQuery" Type="Folder">
				<Item Name="Software Query.vi" Type="VI" URL="../UI/Software Query.vi"/>
			</Item>
			<Item Name="Utility" Type="Folder">
				<Item Name="Format Report Filenames.vi" Type="VI" URL="../UI/Format Report Filenames.vi"/>
				<Item Name="Get Computer Name.vi" Type="VI" URL="../UI/Get Computer Name.vi"/>
			</Item>
		</Item>
		<Item Name="VersionValidator" Type="Folder">
			<Item Name="SemVerValidator.lvclass" Type="LVClass" URL="../SemVerValidator/SemVerValidator.lvclass"/>
			<Item Name="VersionValidator.lvclass" Type="LVClass" URL="../VersionValidator/VersionValidator.lvclass"/>
		</Item>
		<Item Name="TestStand.lvlib" Type="Library" URL="../TestStand/TestStand.lvlib"/>
		<Item Name="Untitled 1.vi" Type="VI" URL="../TestStand/Untitled 1.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="1D String Array to Delimited String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/1D String Array to Delimited String.vi"/>
				<Item Name="Add File to Zip.vi" Type="VI" URL="/&lt;vilib&gt;/zip/Add File to Zip.vi"/>
				<Item Name="Adress_string_in_SS.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Adress_string_in_SS.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Cell_style.ctl" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Gen_styles/Cell_style.ctl"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="CLAUDIE_xlsx.lvlib" Type="Library" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/CLAUDIE_xlsx.lvlib"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Close Zip File.vi" Type="VI" URL="/&lt;vilib&gt;/zip/Close Zip File.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create Directory Recursive.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create Directory Recursive.vi"/>
				<Item Name="Create_app_xml.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Create_app_xml.vi"/>
				<Item Name="Create_content_types.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Create_content_types.vi"/>
				<Item Name="Create_core_xml.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Create_core_xml.vi"/>
				<Item Name="Create_excel.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel.vi"/>
				<Item Name="Create_fills.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Formats/Create_fills.vi"/>
				<Item Name="Create_folder.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Process_folders_VIs/Create_folder.vi"/>
				<Item Name="Create_fonts.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Formats/Create_fonts.vi"/>
				<Item Name="Create_formate_strings.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Formats/Create_formate_strings.vi"/>
				<Item Name="Create_lists_loop.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Create_lists_loop.vi"/>
				<Item Name="Create_rels.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/_rels/Create_rels.vi"/>
				<Item Name="Create_shared_strings_xml.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Create_shared_strings_xml.vi"/>
				<Item Name="Create_sheet_xml.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Create_sheet_xml.vi"/>
				<Item Name="Create_string_list.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Create_string_list.vi"/>
				<Item Name="Create_styles.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Formats/Create_styles.vi"/>
				<Item Name="Create_styles_xml.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Create_styles_xml.vi"/>
				<Item Name="Create_workbook_xml.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Create_workbook_xml.vi"/>
				<Item Name="CreateZip.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Process_folders_VIs/CreateZip.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Doc_props.ctl" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Formats/Typedeffs/Doc_props.ctl"/>
				<Item Name="Enum Registry Keys.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Enum Registry Keys.vi"/>
				<Item Name="Err1_handler.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Errors/Err1_handler.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Font.ctl" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Gen_styles/Font.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Gen_styles_main.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Gen_styles/Gen_styles_main.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate_alph_adress.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Preprocess_Datas/Generate_alph_adress.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get_sheet_names.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Preprocess_Datas/Get_sheet_names.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="Is_num.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Preprocess_Datas/Is_num.vi"/>
				<Item Name="Is_time_stamp.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Preprocess_Datas/Is_time_stamp.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="New Zip File.vi" Type="VI" URL="/&lt;vilib&gt;/zip/New Zip File.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Unzip.lvlib" Type="Library" URL="/&lt;vilib&gt;/zip/NI_Unzip.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Parse_doc_properties.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Parse_doc_properties.vi"/>
				<Item Name="Parse_sheets.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Parse_sheets.vi"/>
				<Item Name="Parse_SS.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Parse_SS.vi"/>
				<Item Name="Parse_styles.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Parse_styles.vi"/>
				<Item Name="Parse_workbook.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Parse_workbook.vi"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Process_path.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Process_folders_VIs/Process_path.vi"/>
				<Item Name="Process_shared_strings_sizes.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Preprocess_Datas/Process_shared_strings_sizes.vi"/>
				<Item Name="ProcessFolder.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Process_folders_VIs/ProcessFolder.vi"/>
				<Item Name="Query Registry Key Info.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Query Registry Key Info.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read_excel.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Reform_num_to_form_string.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Formats/Reform_num_to_form_string.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Relative Path To Platform Independent String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Relative Path To Platform Independent String.vi"/>
				<Item Name="Rename_file.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Process_folders_VIs/Rename_file.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="SharedStrings_string.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Preprocess_Datas/SharedStrings_string.vi"/>
				<Item Name="Sheets_data.ctl" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Create_xmls/Sheets_data.ctl"/>
				<Item Name="Sheets_err.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Errors/Sheets_err.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="SpreadSheet_string.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Preprocess_Datas/SpreadSheet_string.vi"/>
				<Item Name="SS_err.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/Errors/SS_err.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="String_modification.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Preprocess_Datas/String_modification.vi"/>
				<Item Name="Style.ctl" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Gen_styles/Style.ctl"/>
				<Item Name="Style_tables.ctl" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Create_excel/Gen_styles/Style_tables.ctl"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="To_time_stamp.vi" Type="VI" URL="/&lt;vilib&gt;/ATEsystem/CLAUDIE_xlsx/aplikace/Read_excel/To_time_stamp.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Compare Queries.vi" Type="VI" URL="../InstallQuery/Compare Queries.vi"/>
			<Item Name="Comparison.ctl" Type="VI" URL="../SemVerValidator/Comparison.ctl"/>
			<Item Name="Export.vi" Type="VI" URL="../CSVInstallQuery/Export.vi"/>
			<Item Name="Install.ctl" Type="VI" URL="../InstallQuery/Install.ctl"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="JustSoftware" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{B82242C5-B9FB-4522-83EB-DE60E5FABC15}</Property>
				<Property Name="App_INI_GUID" Type="Str">{575346C2-E29C-4C9E-8EDC-0240248D5C01}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{FD85F04C-9149-4236-986F-0553A62EDD6F}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">JustSoftware</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/JustSoftware</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{E5C28CD8-2A97-4AF5-B46D-F470604882ED}</Property>
				<Property Name="Bld_version.build" Type="Int">3</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">JustSoftware.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/JustSoftware/JustSoftware.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/JustSoftware</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{25492CF1-D175-47E7-BF72-A2BF4CBDA87A}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/UI/SoftwareQuery/Software Query.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Support/.rels</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Support/theme1.xml</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
				<Property Name="TgtF_fileDescription" Type="Str">System Query</Property>
				<Property Name="TgtF_internalName" Type="Str">System Query</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2020 </Property>
				<Property Name="TgtF_productName" Type="Str">System Query</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{7B406340-107B-4183-B574-06077CB07372}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">JustSoftware.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="System Query" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{95FFB544-83D4-4F4F-96D5-1466514587B4}</Property>
				<Property Name="App_INI_GUID" Type="Str">{7918D117-2953-4BF6-8016-889B02710E8C}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{15A8114E-65AD-427C-82E4-7A5373929BEE}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">System Query</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/System Query</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{031E1627-9F2D-49F7-B014-900945B067FC}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">System Query.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/System Query/System Query.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/System Query</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{25492CF1-D175-47E7-BF72-A2BF4CBDA87A}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/UI/SoftwareQuery/Software Query.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Support/.rels</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Support/theme1.xml</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
				<Property Name="TgtF_fileDescription" Type="Str">System Query</Property>
				<Property Name="TgtF_internalName" Type="Str">System Query</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2020 </Property>
				<Property Name="TgtF_productName" Type="Str">System Query</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{BB0FF226-7CE2-4856-AEE3-D0420FF34780}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">System Query.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
